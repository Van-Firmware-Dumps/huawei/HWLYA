version : huawei.wakeup.v.1.0.6
committer : zhengwei
date : 2020.9.17
description : 配置文件不使用硬编码加密

version : huawei.wakeup.v.1.0.5
committer : zhengwei
date : 2018.01.08
description : 支持600节点

version : huawei.wakeup.v.1.0.4
committer : zhengwei
date : 2018.01.02
description : 支持800节点

version : huawei.wakeup.v.1.0.3
committer : zhengwei
date : 2018.11.23
description : 你好YOYO修改

version : huawei.wakeup.v.1.0.2
committer : guzhengming
date : 2018.06.27
description : wakeup2.0 enhance

version : huawei.wakeup.v.1.0.1
committer : wangcong
date : 2018.05.16
description : p20 wakeup enhance

version : huawei.wakeup.v.1.0.0
committer : huangli
date : 2017.10.14
description : new
